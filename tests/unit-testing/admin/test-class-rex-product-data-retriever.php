<?php
/**
 * Class Test_Rex_Product_Data_Retriever
 *
 * @package Best_Woocommerce_Feed
 */

/**
 * Test cases for the functions of the production
 * class Rex_Product_Data_Retriever.
 *
 * @see /admin/class-rex-product-data-retriver.php
 */
class Test_Rex_Product_Data_Retriever extends WP_UnitTestCase {
    private static $instance;
    private static $reflector;
    private static $dummy_product;

    public function setUp():void
    {
        self::$dummy_product = WC_Helper_Product::create_simple_product();

        self::$instance      = $this->getMockBuilder( 'Rex_Product_Data_Retriever' )
                                    ->disableOriginalConstructor()
                                    ->getMock();

        self::$reflector     = new ReflectionClass( self::$instance );
    }

    /**
     * @see Rex_Product_Data_Retriever::set_image_att()
     */
    public function test_set_image_att() {
        $image_url = 'http://localhost:8080/wp-content/uploads/woocommerce-placeholder.png';
        $method = self::$reflector->getMethod('set_image_att' );
        $method->setAccessible( true );

        self::$dummy_product->set_image_id(5);

        $product = self::$reflector->getProperty( 'product' );
        $product->setAccessible(true);
        $product->setValue( self::$instance, self::$dummy_product);

        $actual = $method->invoke( self::$instance, 'variation_img' );

        $this->assertSame( $image_url, $actual );
    }

    /**
     * @see Rex_Product_Data_Retriever::tab_replace()
     */
    public function test_tab_replace()
    {
        $sample_val   = '    value    ';
        $expected_val = 'value';

        $method = self::$reflector->getMethod( 'tab_replace' );
        $method->setAccessible( true );

        $product = self::$reflector->getProperty( 'feed_format' );
        $product->setAccessible( true );
        $product->setValue( self::$instance, 'text' );

        $processed_value = $method->invoke( self::$instance, $sample_val );

        $this->assertEquals( $expected_val, $processed_value );
        $this->assertNotEquals( $sample_val, $processed_value );

        $product->setValue( self::$instance, 'xml' );

        $processed_value = $method->invoke( self::$instance, $sample_val );

        $this->assertEquals( $sample_val, $processed_value );
        $this->assertNotEquals( $expected_val, $processed_value );
    }

    /**
     * @see Rex_Product_Data_Retriever::set_woo_discount_rules()
     */
    public function test_set_woo_discount_rules()
    {
        $method = self::$reflector->getMethod( 'set_woo_discount_rules' );
        $method->setAccessible( true );

        $product = self::$reflector->getProperty( 'product' );
        $product->setAccessible( true );
        $product->setValue( self::$instance, self::$dummy_product );

        $discounted_value = $method->invoke( self::$instance, 'woo_discount_rules_price' );

        $this->assertIsNumeric( $discounted_value );
    }
}
