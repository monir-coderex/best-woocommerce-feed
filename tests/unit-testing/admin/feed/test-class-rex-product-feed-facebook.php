<?php
/**
 * Class Test_Rex_Product_Feed_Facebook
 *
 * @package Best_Woocommerce_Feed
 */

/**
 * Test cases for the functions of the production
 * class Rex_Product_Feed_Facebook.
 *
 * @see /admin/feed/class-rex-product-feed-facebook.php
 */
class Test_Rex_Product_Feed_Facebook extends WP_UnitTestCase {
    private static $instance;
    private static $reflector;

    public function setUp():void
    {
        self::$instance      = $this->getMockBuilder( 'Rex_Product_Feed_Facebook' )
                                    ->disableOriginalConstructor()
                                    ->getMock();

        self::$reflector     = new ReflectionClass( self::$instance );
    }

    /**
     * @see Rex_Product_Feed_Facebook::process_attributes_for_shipping_tax()
     */
    public function test_process_attributes_for_shipping_tax() {
        $attr_before = self::get_dummy_attributes();
        $method = self::$reflector->getMethod('process_attributes_for_shipping_tax' );
        $method->setAccessible( true );

        $attr_after = $method->invoke( self::$instance, $attr_before );

        $this->assertTrue( isset( $attr_after[ 'shipping' ] ) );
        $this->assertTrue( isset( $attr_after[ 'shipping' ][ 'shipping_country' ] ) );
        $this->assertNotTrue( isset( $attr_after[ 'shipping_country' ] ) );
        $this->assertTrue( isset( $attr_after[ 'tax' ] ) );
        $this->assertTrue( isset( $attr_after[ 'tax' ][ 'tax_country' ] ) );
        $this->assertNotTrue( isset( $attr_after[ 'tax_country' ] ) );
        $this->assertNotEmpty( $attr_after[ 'tax' ][ 'tax_country' ] );
        $this->assertNotEmpty( $attr_after[ 'shipping' ][ 'shipping_country' ] );
        $this->assertNotEquals( $attr_before, $attr_after );
    }

    /**
     * Get attributes before processing
     * @return array
     */
    private static function get_dummy_attributes()
    {
        return array(
            'id'               => 14,
            'title'            => 'Hoodie',
            'shipping_country' => array
            (
                0 => 'Bangladesh'
            ),
            'tax_country'      => 'Bangladesh'
        );
    }
}