<?php
/**
 * Class Test_Rex_Product_Feed_Abstract_Generator
 *
 * @package Best_Woocommerce_Feed
 */

/**
 * Test cases for the functions of the production
 * class Rex_Product_Feed_Abstract_Generator.
 *
 * @see /admin/feed/abstract-rex-product-feed-generator.php
 */
class Test_Rex_Product_Feed_Abstract_Generator extends WP_UnitTestCase {
    private static $instance;
    private static $reflector;

    public function setUp():void
    {
        self::$instance      = $this->getMockBuilder( 'Rex_Product_Feed_Abstract_Generator' )
                                    ->disableOriginalConstructor()
                                    ->getMock();

        self::$reflector     = new ReflectionClass( self::$instance );
    }

    /**
     * @see Rex_Product_Feed_Abstract_Generator::get_feed_format()
     */
    public function test_get_feed_format() {
        $expected_feed_format = 'xml';
        $method = self::$reflector->getMethod('get_feed_format' );

        $feed_format = self::$reflector->getProperty( 'feed_format' );
        $feed_format->setAccessible(true);
        $feed_format->setValue( self::$instance, $expected_feed_format);

        $actual_feed_format = $method->invoke( self::$instance );

        $this->assertEquals( $expected_feed_format, $actual_feed_format );
        $this->assertNotEquals( 'xmls', $actual_feed_format );
    }
}