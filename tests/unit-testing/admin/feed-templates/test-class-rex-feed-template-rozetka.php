<?php
/**
 * Class Test_Rex_Feed_Template_Rozetka
 *
 * @package Best_Woocommerce_Feed
 */

/**
 * Test cases for the functions of the production
 * class Rex_Feed_Template_Rozetka.
 *
 * @see /admin/feed-templates/class-rex-feed-template-Rozetka.php
 */
class Test_Rex_Feed_Template_Rozetka extends WP_UnitTestCase {
    private static $instance;
    private static $reflector;

    public function setUp():void
    {
        self::$instance      = $this->getMockBuilder( 'Rex_Feed_Template_Rozetka' )
                                    ->disableOriginalConstructor()
                                    ->getMock();

        self::$reflector     = new ReflectionClass( self::$instance );
    }

    /**
     * @see Rex_Feed_Template_Rozetka::init_atts()
     */
    public function test_init_atts()
    {
        $expected_attr = self::get_dummy_template_attr_Rozetka();
        $init_method   = self::$reflector->getMethod( 'init_atts' );
        $init_method->setAccessible( true );
        $attributes = self::$reflector->getProperty( 'attributes' );
        $attributes->setAccessible( true );
        $attributes->setValue( self::$instance, $expected_attr );
        $actual_attr = $attributes->getValue( self::$instance );

        self::assertEquals( $expected_attr, $actual_attr );

        $expected_attr_mappings = self::get_dummy_template_mappings_Rozetka();
        $init_method            = self::$reflector->getMethod( 'init_default_template_mappings' );
        $init_method->setAccessible( true );
        $attr_mappings = self::$reflector->getProperty( 'template_mappings' );
        $attr_mappings->setAccessible( true );
        $attr_mappings->setValue( self::$instance, $expected_attr_mappings );
        $actual_attr_mappings = $attr_mappings->getValue( self::$instance );

        self::assertEquals( $expected_attr_mappings, $actual_attr_mappings );
    }

    /**
     * Get attributes before processing
     * @return array
     */
    private static function get_dummy_template_attr_rozetka()
    {
        return array(
            'Required Information' => array(
                'stock_quantity' => 'Quantity in Stock [stock_quantity]',
                'price'          => 'Price [price]',
                'currencyId'     => 'Currency ID [currencyId]',
                'categoryId'     => 'Category ID [categoryId]',
                'picture'        => 'Image [picture]',
                'vendor'         => 'Vendor [vendor]',
                'name'           => 'Product Name [name]',
                'description'    => 'Product Description [description]',
                'param'          => 'Parameter [param]'
            ),
            'Optional Information' => array(
                'url'       => 'Product URL',
                'price_old' => 'Old Price [price_old]',
                'name_ua'   => 'Ukrainian Product Name [name_ua]',
                'state'     => 'Product Condition [state]',
            )
        );
    }

    /**
     * Get attribute mappings before processing
     * @return array
     */
    private static function get_dummy_template_mappings_rozetka()
    {
        return array(
            array(
                'attr'     => 'stock_quantity',
                'type'     => 'meta',
                'meta_key' => 'quantity',
                'st_value' => '',
                'prefix'   => '',
                'suffix'   => '',
                'escape'   => 'default',
                'limit'    => 0,
            ),
            array(
                'attr'     => 'price',
                'type'     => 'meta',
                'meta_key' => 'price',
                'st_value' => '',
                'prefix'   => '',
                'suffix'   => '',
                'escape'   => 'default',
                'limit'    => 0,
            ),
            array(
                'attr'     => 'currencyId',
                'type'     => 'meta',
                'meta_key' => '',
                'st_value' => '',
                'prefix'   => '',
                'suffix'   => '',
                'escape'   => 'default',
                'limit'    => 0,
            ),
            array(
                'attr'     => 'categoryId',
                'type'     => 'meta',
                'meta_key' => '',
                'st_value' => '',
                'prefix'   => '',
                'suffix'   => '',
                'escape'   => 'default',
                'limit'    => 0,
            ),
            array(
                'attr'     => 'picture',
                'type'     => 'meta',
                'meta_key' => 'main_image',
                'st_value' => '',
                'prefix'   => '',
                'suffix'   => '',
                'escape'   => 'default',
                'limit'    => 0,
            ),
            array(
                'attr'     => 'vendor',
                'type'     => 'static',
                'meta_key' => '',
                'st_value' => '',
                'prefix'   => '',
                'suffix'   => '',
                'escape'   => 'default',
                'limit'    => 0,
            ),
            array(
                'attr'     => 'name',
                'type'     => 'meta',
                'meta_key' => 'title',
                'st_value' => '',
                'prefix'   => '',
                'suffix'   => '',
                'escape'   => 'default',
                'limit'    => 0,
            ),
            array(
                'attr'     => 'description',
                'type'     => 'meta',
                'meta_key' => 'description',
                'st_value' => '',
                'prefix'   => '',
                'suffix'   => '',
                'escape'   => 'default',
                'limit'    => 0,
            ),
            array(
                'attr'     => 'param',
                'type'     => 'meta',
                'meta_key' => '',
                'st_value' => '',
                'prefix'   => '',
                'suffix'   => '',
                'escape'   => 'default',
                'limit'    => 0,
            )
        );
    }
}