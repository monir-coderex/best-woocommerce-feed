<?php
/**
 * Class Test_Rex_Feed_Template_Hotline
 *
 * @package Best_Woocommerce_Feed
 */

/**
 * Test cases for the functions of the production
 * class Rex_Feed_Template_Hotline.
 *
 * @see /admin/feed-templates/class-rex-feed-template-hotline.php
 */
class Test_Rex_Feed_Template_Hotline extends WP_UnitTestCase {
    private static $instance;
    private static $reflector;

    public function setUp():void
    {
        self::$instance      = $this->getMockBuilder( 'Rex_Feed_Template_Hotline' )
                                    ->disableOriginalConstructor()
                                    ->getMock();

        self::$reflector     = new ReflectionClass( self::$instance );
    }

    /**
     * @see Rex_Feed_Template_Hotline::init_atts()
     */
    public function test_init_atts()
    {
        $expected_attr = self::get_dummy_template_attr_hotline();
        $init_method   = self::$reflector->getMethod( 'init_atts' );
        $init_method->setAccessible( true );
        $attributes = self::$reflector->getProperty( 'attributes' );
        $attributes->setAccessible( true );
        $attributes->setValue( self::$instance, $expected_attr );
        $actual_attr = $attributes->getValue( self::$instance );

        self::assertEquals( $expected_attr, $actual_attr );

        $expected_attr_mappings = self::get_dummy_template_mappings_hotline();
        $init_method            = self::$reflector->getMethod( 'init_default_template_mappings' );
        $init_method->setAccessible( true );
        $attr_mappings = self::$reflector->getProperty( 'template_mappings' );
        $attr_mappings->setAccessible( true );
        $attr_mappings->setValue( self::$instance, $expected_attr_mappings );
        $actual_attr_mappings = $attr_mappings->getValue( self::$instance );

        self::assertEquals( $expected_attr_mappings, $actual_attr_mappings );
    }

    /**
     * Get attributes before processing
     * @return array
     */
    private static function get_dummy_template_attr_hotline()
    {
        return array(
            'Required Information'   => array(
                'id'         => 'Product ID [id]',
                'categoryId' => 'Category ID [categoryId]',
                'code'       => 'Product Model [code]',
                'barcode'    => 'Product Barcode [barcode]',
                'vendor'     => 'Manufacturer [vendor]',
                'name'       => 'Product Name [name]',
                'url'        => 'Product URL [url]',
                'priceRUAH'  => 'Price RUAH [priceRUAH]',
            ),
            'Additional Information' => array(
                'code'        => 'Product Model [code]',
                'barcode'     => 'Product Barcode [barcode]',
                'description' => 'Product Description [description]',
                'image'       => 'Product Image URL [image]',
                'priceRUSD'   => 'Price RUSD [priceRUSD]',
                'stock'       => 'Availability [stock]',
            ),
        );
    }

    /**
     * Get attribute mappings before processing
     * @return array
     */
    private static function get_dummy_template_mappings_hotline()
    {
        return array(
            array(
                'attr'     => 'id',
                'type'     => 'meta',
                'meta_key' => 'id',
                'st_value' => '',
                'prefix'   => '',
                'suffix'   => '',
                'escape'   => 'default',
                'limit'    => 0,
            ),
            array(
                'attr'     => 'categoryId',
                'type'     => 'meta',
                'meta_key' => 'product_cat_ids',
                'st_value' => '',
                'prefix'   => '',
                'suffix'   => '',
                'escape'   => 'default',
                'limit'    => 0,
            ),
            array(
                'attr'     => 'code',
                'type'     => 'meta',
                'meta_key' => '',
                'st_value' => '',
                'prefix'   => '',
                'suffix'   => '',
                'escape'   => 'default',
                'limit'    => 0,
            ),
            array(
                'attr'     => 'barcode',
                'type'     => 'meta',
                'meta_key' => 'sku',
                'st_value' => '',
                'prefix'   => '',
                'suffix'   => '',
                'escape'   => 'default',
                'limit'    => 0,
            ),
            array(
                'attr'     => 'vendor',
                'type'     => 'static',
                'meta_key' => '',
                'st_value' => '',
                'prefix'   => '',
                'suffix'   => '',
                'escape'   => 'default',
                'limit'    => 0,
            ),
            array(
                'attr'     => 'name',
                'type'     => 'meta',
                'meta_key' => 'title',
                'st_value' => '',
                'prefix'   => '',
                'suffix'   => '',
                'escape'   => 'default',
                'limit'    => 0,
            ),
            array(
                'attr'     => 'url',
                'type'     => 'meta',
                'meta_key' => 'link',
                'st_value' => '',
                'prefix'   => '',
                'suffix'   => '',
                'escape'   => 'default',
                'limit'    => 0,
            ),
            array(
                'attr'     => 'priceRUAH',
                'type'     => 'meta',
                'meta_key' => 'price',
                'st_value' => '',
                'prefix'   => '',
                'suffix'   => ' ' . get_option('woocommerce_currency'),
                'escape'   => 'default',
                'limit'    => 0,
            ),
        );
    }
}