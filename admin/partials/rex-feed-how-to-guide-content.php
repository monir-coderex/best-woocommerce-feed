<h2 class="rex-feed-how-to-guide-header"><?php esc_html_e( 'How To Guide', 'rex-product-feed' )?> </h2>
<ol class="rex-feed-how-to-guide-lists">
    <li>
        <a href="https://rextheme.com/docs/wpfm-generate-product-feed/" target="_blank">
            <?php esc_html_e( 'Generate a product feed for the first time', 'rex-product-feed' )?>
        </a>
    </li>
    <li>
        <a href="https://rextheme.com/docs/how-to-generate-woocommerce-product-feed-for-google/" target="_blank">
            <?php esc_html_e( 'Generate a product feed for Google', 'rex-product-feed' )?>
        </a>
    </li>
    <li>
        <a href="https://rextheme.com/docs/how-to-generate-woocommerce-product-feed-for-google/" target="_blank">
            <?php esc_html_e( 'Generate WooCommerce product feed for Google', 'rex-product-feed' )?>
        </a>
    </li>
    <li>
        <a href="https://rextheme.com/docs/how-to-auto-sync-product-feed-to-google-merchant-shop/" target="_blank">
            <?php esc_html_e( 'Google Merchant Auto-sync', 'rex-product-feed' )?>
        </a>
    </li>
    <li>
        <a href="https://rextheme.com/docs/how-to-generate-woocommerce-product-feed-for-facebook/" target="_blank">
            <?php esc_html_e( 'Generate product feed for Facebook', 'rex-product-feed' )?>
        </a>
    </li>
    <li>
        <a href="https://rextheme.com/docs/wpfm-category-mapping-product-feed/" target="_blank">
            <?php esc_html_e( 'How to use Category Mapping', 'rex-product-feed' )?>
        </a>
    </li>
    <li>
        <a href="https://rextheme.com/docs/wpfm-extensive-custom-fields-woocommerce-products/" target="_blank">
            <?php esc_html_e( 'Enable custom fields (Pro)', 'rex-product-feed' )?>
        </a>
    </li>
</ol>

<h2 class="rex-feed-how-to-guide-header"><?php esc_html_e( 'Common Troubleshooting Issue', 'rex-product-feed' )?> </h2>
<ol class="rex-feed-how-to-guide-lists">
    <li>
        <a href="https://rextheme.com/docs/wpfm-troubleshooting-for-common-issues/#error-on-top-of-feed" target="_blank">
            <?php esc_html_e( 'Error on line x at column x: Extra content at the end of the document', 'rex-product-feed' )?>
        </a>
    </li>
    <li>
        <a href="https://rextheme.com/docs/google-merchant-center-issues/" target="_blank">
            <?php esc_html_e( 'Product Missing on Google Merchant Center/ Not uploaded', 'rex-product-feed' )?>
        </a>
    </li>
    <li>
        <a href="https://rextheme.com/docs/facebook-commerce-manager-issues/" target="_blank">
            <?php esc_html_e( 'Product Missing on Facebook Commerce Manager/ Not uploaded', 'rex-product-feed' )?>
        </a>
    </li>
    <li>
        <a href="https://rextheme.com/docs/wpfm-troubleshooting-for-common-issues/#access-token-expired-error-message" target="_blank">
            <?php esc_html_e( '"Your access token has expired" error message', 'rex-product-feed' )?>
        </a>
    </li>
    <li>
        <a href="https://rextheme.com/docs/wpfm-troubleshooting-for-common-issues/#description-sanitization" target="_blank">
            <?php esc_html_e( 'Remove Tags, Shortcodes, or HTML tags', 'rex-product-feed' )?>
        </a>
    </li>
    <li>
        <a href="https://rextheme.com/docs/how-to-generate-product-feed-with-wpml-translated-product-data/" target="_blank">
            <?php esc_html_e( 'Generate feed in different languages', 'rex-product-feed' )?>
        </a>
    </li>
    <li>
        <a href="https://rextheme.com/docs-category/product-feed-manager/" target="_blank">
            <?php esc_html_e( 'View All Docs', 'rex-product-feed' )?>
        </a>
    </li>
</ol>